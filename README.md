
# README #

This filter driver intercepts Windows Management Instrumentation (WMI) query to the kernel driver (WMIxWDM). 

The purpose of the interception will modify the string "vmware" or "virtual" string returned as a result of the WMI query.

# TECHNICAL DETAILS #

Long story short, the real dirty work was done in *WmiFilterSystemControlComplete* hooked on *IRP_MJ_SYSTEM_CONTROL* dispatch routine