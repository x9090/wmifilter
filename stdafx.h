#ifdef __cplusplus
extern "C" 
{
#endif

#if DBG == 1
#define DEBUG 1
#else
#define DEBUG 0
#endif	

#ifdef __cplusplus
}
#endif

// WMI driver name
#define WMI_DRIVER_NAME  L"\\Driver\\WMIxWDM"
#define MAX_PATH 256

#ifdef __cplusplus
extern "C" 
{

#endif

#include <wdm.h>
#include <ntddk.h>
#include <ntddstor.h>
#include <mountdev.h>
#include <ntddvol.h>
#include <Wmistr.h>
#include <wchar.h>

extern POBJECT_TYPE IoDriverObjectType;

/* Undocumented Windows native API */
NTSTATUS ObReferenceObjectByName(
	PUNICODE_STRING ObjectName,
	ULONG Attributes,
	PACCESS_STATE AccessState,
	ACCESS_MASK DesiredAccess,
	POBJECT_TYPE ObjectType,
	KPROCESSOR_MODE AccessMode,
	PVOID ParseContext,
	PVOID *Object);

#ifdef __cplusplus
}
#endif
