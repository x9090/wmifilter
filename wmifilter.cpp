#include "stdafx.h"
/*
WmiFilter - Main file
This file contains a very simple implementation of a WDM driver. Note that it does not support all
WDM functionality, or any functionality sufficient for practical use. The only thing this driver does
perfectly, is loading and unloading.

To install the driver, go to Control Panel -> Add Hardware Wizard, then select "Add a new hardware device".
Select "manually select from list", choose device category, press "Have Disk" and enter the path to your
INF file.
Note that not all device types (specified as Class in INF file) can be installed that way.

To start/stop this driver, use Windows Device Manager (enable/disable device command).

If you want to speed up your driver development, it is recommended to see the BazisLib library, that
contains convenient classes for standard device types, as well as a more powerful version of the driver
wizard. To get information about BazisLib, see its website:
http://bazislib.sysprogs.org/
*/

/* Our function prototypes */
VOID DebugBreak();
VOID WmiFilterUnload(IN PDRIVER_OBJECT DriverObject);
NTSTATUS WmiFilterCreateClose(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS WmiFilterDefaultHandler(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS WmiFilterAttachDevice(IN PDRIVER_OBJECT  DriverObject);
NTSTATUS WmiFilterPnP(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS WmiFilterPower(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS WmiFilterDispatchSystemControl(IN PDEVICE_OBJECT, IN PIRP Irp);
NTSTATUS WmiFilterSystemControlComplete(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp, IN PVOID Context);

typedef struct _deviceExtension
{
	PDEVICE_OBJECT TopOfStack;
} WmiFilter_DEVICE_EXTENSION, *PWmiFilter_DEVICE_EXTENSION;

#ifdef __cplusplus
extern "C" NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING  RegistryPath);
#endif

VOID __declspec(naked) DebugBreak()
{

	__asm {
		pushad
			mov ebx, DEBUG
			xor eax, eax
			cmp eax, ebx
			jz quit
			int 3
		quit:
		popad
			retn
	}
}

NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING  RegistryPath)
{
	unsigned i;

	KdPrint(("[%s] Driver entry point!\n", __FUNCTION__));

	for (i = 0; i <= IRP_MJ_MAXIMUM_FUNCTION; i++)
		DriverObject->MajorFunction[i] = (PDRIVER_DISPATCH)WmiFilterDefaultHandler;

	// 
	// Setting these major functions caused the keyboard not responding as the 
	// dispatch routine has called IoCompleteRequest!!
	//
	/*DriverObject->MajorFunction[IRP_MJ_CREATE] = WmiFilterCreateClose;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = WmiFilterCreateClose;*/

	//
	// System control is where we do our real work.
	//
	DriverObject->MajorFunction[IRP_MJ_SYSTEM_CONTROL] = (PDRIVER_DISPATCH)WmiFilterDispatchSystemControl;

	//
	// The only reason we need to handle PnP IRPs is to know when
	// a device we've attached to disappears (is removed).
	//
	DriverObject->MajorFunction[IRP_MJ_PNP] = (PDRIVER_DISPATCH)WmiFilterPnP;
	//
	// Power IRPs are the only ones we have to handle specially under
	// Win2k since they require the special PoCallDriver and 
	// PoStartNextPowerIrp function calls.
	//
	DriverObject->MajorFunction[IRP_MJ_POWER] = (PDRIVER_DISPATCH)WmiFilterPower;

	//
	// Under Win2K we get told about the presence of keyboard 
	// devices through our AddDevice entry point.
	//
	DriverObject->DriverUnload = WmiFilterUnload;
	DriverObject->DriverStartIo = NULL;
	// AddDevice callback function gets triggered via PnP
	//DriverObject->DriverExtension->AddDevice = WmiFilterAttachDevice;

	// Manually attach our filter driver to \Device\WMIDataDevice
	WmiFilterAttachDevice(DriverObject);

	// If not using PnP, we can manually attach our filter driver to keyboard device
	return STATUS_SUCCESS;
}

//----------------------------------------------------------------------
// 
// WmiFilterUnload
//
// Our Win2K PnP unload function. We don't need to do anything.
//
// Called: WIN2K
//
//----------------------------------------------------------------------
VOID WmiFilterUnload(IN PDRIVER_OBJECT Driver)
{
	KdPrint(("[%s] Unloading driver..\n", __FUNCTION__));

	UNREFERENCED_PARAMETER(Driver);

	ASSERT(NULL == Driver->DeviceObject);
}


NTSTATUS WmiFilterCreateClose(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
	KdPrint(("[%s] Called!\n", __FUNCTION__));
	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS WmiFilterDefaultHandler(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
	PWmiFilter_DEVICE_EXTENSION deviceExtension = NULL;

	// Default dispatch handler not being handled
	// Pass IRP to the next lower device object
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_TRACE_LEVEL, "[%s] Skip IRP.\n", __FUNCTION__));
	IoSkipCurrentIrpStackLocation(Irp);
	deviceExtension = (PWmiFilter_DEVICE_EXTENSION)DeviceObject->DeviceExtension;
	return IoCallDriver(deviceExtension->TopOfStack, Irp);
}

//----------------------------------------------------------------------
//
// WmiFilterAttachDevice
//
// The PnP Manager calls us for each keyboard present on the system.
// We attach to each one so that we can flip caps lock to controls.
//
// Called: WIN2K
//
//----------------------------------------------------------------------
NTSTATUS WmiFilterAttachDevice(IN PDRIVER_OBJECT Driver)
{
	PWmiFilter_DEVICE_EXTENSION	pExt;
	UNICODE_STRING	uniNtNameString;
	PDEVICE_OBJECT	pFilterDeviceObject;
	PDEVICE_OBJECT	pWmiDeviceObject;
	PDEVICE_OBJECT	pTargetDeviceObject;
	PDRIVER_OBJECT	pWmiDriverObject;
	NTSTATUS		status = STATUS_SUCCESS;

	KdPrint(("[%s] Called!\n", __FUNCTION__));
	//DebugBreak();

	RtlInitUnicodeString(&uniNtNameString, WMI_DRIVER_NAME);

	//
	// Get WMI driver object
	//
	status = ObReferenceObjectByName(
		&uniNtNameString,
		OBJ_CASE_INSENSITIVE,
		NULL,
		0,
		IoDriverObjectType,
		KernelMode,
		NULL,
		(PVOID*)&pWmiDriverObject
		);

	if (!NT_SUCCESS(status))
	{
		KdPrint(("[%s] Failed calling ObReferenceObjectByName\n", __FUNCTION__));
		return (status);
	}

	//
	// Get WMI device object
	//
	pWmiDeviceObject = pWmiDriverObject->DeviceObject;

	//
	// Create a filter device and attach it to the device stack.
	// Our filter device must have the same attribute/characteristic as the target device object
	//
	status = IoCreateDevice(Driver,
		sizeof(WmiFilter_DEVICE_EXTENSION),
		NULL,
		pWmiDeviceObject->DeviceType,
		pWmiDeviceObject->Characteristics,
		FALSE,
		&pFilterDeviceObject
		);


	if (!NT_SUCCESS(status))
	{
		KdPrint(("[%s] Unable to create our filter device\n", __FUNCTION__));
		return (status);
	}
	else
	{
		// We don't need WMI driver object anymore
		ObDereferenceObject(pWmiDriverObject);
	}

	//
	// Initialize our filter device extension value
	//
	RtlZeroMemory(pFilterDeviceObject->DeviceExtension, sizeof(WmiFilter_DEVICE_EXTENSION));

	pTargetDeviceObject = pWmiDeviceObject;
	pExt = (PWmiFilter_DEVICE_EXTENSION)pFilterDeviceObject->DeviceExtension;
	pExt->TopOfStack = pTargetDeviceObject;
	ASSERT(pExt->TopOfStack);
	pFilterDeviceObject->Flags |= pTargetDeviceObject->Flags;

	//
	// The return value is the attached device object (AKA lower device object)
	//
	pTargetDeviceObject = IoAttachDeviceToDeviceStack(pFilterDeviceObject, pWmiDeviceObject);

	if (pTargetDeviceObject != pWmiDeviceObject)
	{
		KdPrint(("[%s] Not the target device that we are attaching.\n", __FUNCTION__));
		IoDetachDevice(pTargetDeviceObject);
	}

	return status;
}


NTSTATUS WmiFilterIrpCompletion(
	IN PDEVICE_OBJECT DeviceObject,
	IN PIRP Irp,
	IN PVOID Context
	)
{
	PKEVENT Event = (PKEVENT)Context;

	UNREFERENCED_PARAMETER(DeviceObject);
	UNREFERENCED_PARAMETER(Irp);

	KeSetEvent(Event, IO_NO_INCREMENT, FALSE);

	return(STATUS_MORE_PROCESSING_REQUIRED);
}

//----------------------------------------------------------------------
//
// WmiFilterPnP
//
// We have to handle PnP IRPs so that we detach from target
// devices when appropriate.
//
// Called: WIN2K
//
//----------------------------------------------------------------------
NTSTATUS WmiFilterPnP(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
	PIO_STACK_LOCATION irpSp = IoGetCurrentIrpStackLocation(Irp);
	PWmiFilter_DEVICE_EXTENSION pExt = ((PWmiFilter_DEVICE_EXTENSION)DeviceObject->DeviceExtension);
	NTSTATUS status = STATUS_SUCCESS;

	KdPrint(("[%s] Called!\n", __FUNCTION__));

	ASSERT(pExt);

	switch (irpSp->MinorFunction)
	{
		// The device is being removed (plug-n-play)
	case IRP_MN_REMOVE_DEVICE:

		KdPrint(("[%s] IRP_MN_REMOVE_DEVICE\n", __FUNCTION__));

		//
		// First pass the IRP to the next driver
		//
		IoSkipCurrentIrpStackLocation(Irp);
		status = IoCallDriver(pExt->TopOfStack, Irp);
		//
		// Detach our filter driver
		//
		IoDetachDevice(pExt->TopOfStack);
		//
		// Delete our device object
		//
		IoDeleteDevice(DeviceObject);
		break;
	case IRP_MN_SURPRISE_REMOVAL:
		//
		// Same as a remove device, but don't call IoDetach or IoDeleteDevice.
		//
		KdPrint(("[%s] IRP_MN_SURPRISE_REMOVAL\n", __FUNCTION__));
		IoSkipCurrentIrpStackLocation(Irp);
		status = IoCallDriver(pExt->TopOfStack, Irp);
		break;
	default:
		//
		// Pass these through untouched
		//
		KdPrint(("[%s] Default\n", __FUNCTION__));
		IoSkipCurrentIrpStackLocation(Irp);
		status = IoCallDriver(pExt->TopOfStack, Irp);
		break;
	}
	return status;
}

//----------------------------------------------------------------------
//
// WmiFilterPower
//
// We have to handle Power IRPs specially.
//
// Called: WIN2K
//
//----------------------------------------------------------------------
NTSTATUS WmiFilterPower(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
	PWmiFilter_DEVICE_EXTENSION pExt;
	pExt = (PWmiFilter_DEVICE_EXTENSION)DeviceObject->DeviceExtension;

	KdPrint(("[%s] Called!\n", __FUNCTION__));
	ASSERT(pExt);

	// Purpose?
	PoStartNextPowerIrp(Irp);
	IoSkipCurrentIrpStackLocation(Irp);
	return PoCallDriver(pExt->TopOfStack, Irp);
}

//----------------------------------------------------------------------
//
// WmiFilterDispatchSystemControl
//
// Sets up to look at the read request completion so that we can
// massage the input queue on IO completion.
//
// Called: WIN2K, NT4
//
//----------------------------------------------------------------------
NTSTATUS WmiFilterDispatchSystemControl(
	IN PDEVICE_OBJECT DeviceObject,
	IN PIRP Irp)
{
	PWmiFilter_DEVICE_EXTENSION   devExt;

	KdPrint(("[%s] Called!\n", __FUNCTION__));
	//DebugBreak();


	if (Irp->CurrentLocation == 1)
	{
		ULONG ReturnedInformation = 0;
		NTSTATUS status;
		KdPrint(("[%s] Dispatch encountered bogus current location\n", __FUNCTION__));
		status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Status = status;
		Irp->IoStatus.Information = ReturnedInformation;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return(status);
	}
	//
	// Gather our variables.
	//
	devExt = (PWmiFilter_DEVICE_EXTENSION)DeviceObject->DeviceExtension;

	//
	// Push params down for WMI driver.
	//
	IoCopyCurrentIrpStackLocationToNext(Irp);

	//  
	// Set the completion callback, so we can tweak the WMI result
	//	    
	IoSetCompletionRoutine(Irp, WmiFilterSystemControlComplete, DeviceObject, TRUE, TRUE, TRUE);

	//
	// Return the results of the call to the WMI driver.
	//
	return IoCallDriver(devExt->TopOfStack, Irp);
}

//----------------------------------------------------------------------
// 
// WmiFilterSystemControlComplete
//
// Gets control after a read operation has completed.
//
// Purpose: Filter all related vmware/virtual related string upon completion of IRP request
// 
// Note: 
//		: To avoid Win32_DiskDrive:
//			- IMPORTANT! Replacing the string for example the VMWare SCSI device name will cause WMI failed to find the correct
//            property name (eg: PNPDeviceID, Caption, Model) from Win32_DiskDrive instance
//			- More information: cimwin32!CWin32DiskDrive::GetPhysDiskInfoNT -> cimwin32!CConfigManager::LocateDevice -> This function will
//			  will fail to locate the SCSI device and hence the following properties value cannot be located:
//				* IDS_Description
//				* IDS_Caption
//				* IDS_Model
//				* IDS_Manufacturer
//			- On Windows 7, it doesn't send this IRP to IRP_MJ_SYSTEM_CONTROL anymore. Instead, it retrieved this the disk drive info from the 
//			  registry HKLM\\SYSTEM\\CurrentControlSet\\Enum\\SCSI\\Disk&Ven_VMware_&Prod_VMware_Virtual_S
//		: To avoid Win32_CDROMDrive check, we can just disable ROM drive on the VM image
//		: Win32_BIOS
//
//----------------------------------------------------------------------
NTSTATUS WmiFilterSystemControlComplete(
	IN PDEVICE_OBJECT DeviceObject,
	IN PIRP Irp,
	IN PVOID Context
	)
{
	PIO_STACK_LOCATION	IrpSp;
	int	i;

	KdPrint((__FUNCTION__ " entered!\n"));

	//
	// Request completed - look at the result.
	//
	IrpSp = IoGetCurrentIrpStackLocation(Irp);
	if (NT_SUCCESS(Irp->IoStatus.Status))
	{
		//
		// WMI request is sent via IRP_MJ_SYSTEM_CONTROL
		// We are only interested on WMI query request
		//
		if (IrpSp->MajorFunction == IRP_MJ_SYSTEM_CONTROL &&
			(IrpSp->MinorFunction == IRP_MN_QUERY_SINGLE_INSTANCE ||
			IrpSp->MinorFunction == IRP_MN_QUERY_ALL_DATA))
		{
			switch (IrpSp->MinorFunction)
			{
			case IRP_MN_QUERY_SINGLE_INSTANCE:
			{
				PWNODE_SINGLE_INSTANCE pSingleInstance = (PWNODE_SINGLE_INSTANCE)IrpSp->Parameters.WMI.Buffer;
				ULONG dwInstSize = IrpSp->Parameters.WMI.BufferSize;
				ULONG dwBlockSize = pSingleInstance->SizeDataBlock;
				ULONG dwBlockOffset = pSingleInstance->DataBlockOffset;
				ULONG dwNumInstance = pSingleInstance->InstanceIndex;
				// The first word is the size instance name
				// [SizeOfInstanceName:WORD][wInstanceName]
				PCHAR pDataBlock = (PCHAR)pSingleInstance + dwBlockOffset + 2;
				KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE execute\n", __FUNCTION__));
				KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE -> pSingleInstance: 0x%p\n", __FUNCTION__, pSingleInstance));
				KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE -> pDataBlock: 0x%p\n", __FUNCTION__, pDataBlock));
				KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE -> Data block size: 0x%p\n", __FUNCTION__, dwBlockSize));
				KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE -> Instance size: 0x%08x\n", __FUNCTION__, dwInstSize));
				KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE -> Instance index: 0x%08x\n", __FUNCTION__, dwNumInstance));

				//
				// Find VMware string
				//
				WCHAR *pSubStr = wcsstr((WCHAR*)pDataBlock, L"VMware");

				if (pSubStr != NULL)
				{
					KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE -> Found vmware string\n", __FUNCTION__));
					//
					// Replace the VMware related string
					//
					/*int start = pSubStr - cstr;
					std::string cppstr(cstr);
					cppstr.replace(start, strlen("vmware"), "MYTUAL");*/
					do{
						wmemcpy(pSubStr, L"MYTUAL", wcslen(L"VMware"));
						pSubStr = wcsstr((WCHAR*)pSubStr, L"VMware");
					} while (pSubStr != NULL);
				}

				//
				// Find virtual string
				//
				pSubStr = wcsstr((WCHAR*)pDataBlock, L"Virtual");

				if (pSubStr != NULL)
				{
					KdPrint(("[%s] IRP_MN_QUERY_SINGLE_INSTANCE -> Found virtual string\n", __FUNCTION__));
					//
					// Replace the virtual related string
					//
					/*int start = pSubStr - cstr;
					std::string cppstr(cstr);
					cppstr.replace(start, strlen("virtual"), "MYVTUAL");*/
					do{
						wmemcpy(pSubStr, L"MYVTUAL", wcslen(L"Virtual"));
						pSubStr = wcsstr((WCHAR*)pSubStr, L"Virtual");
					} while (pSubStr != NULL);
				}

				break;
			}
			//
			// This request will already be sent during OS bootup
			// This request will be sent ONCE and probably cached. Further request will not trigger this event (WINXP)
			//
			case IRP_MN_QUERY_ALL_DATA:
			{
				PWNODE_ALL_DATA pAllData = (PWNODE_ALL_DATA)IrpSp->Parameters.WMI.Buffer;
				ULONG dwBufferSize = IrpSp->Parameters.WMI.BufferSize;
				ULONG dwDataBlockOffset = pAllData->DataBlockOffset;
				ULONG dwFlags = pAllData->WnodeHeader.Flags;
				ULONG dwFixedInstsz;

				KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA execute\n", __FUNCTION__));
				KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> pAllData: 0x%p\n", __FUNCTION__, pAllData));
				KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> Buffer size: 0x%08x\n", __FUNCTION__, dwBufferSize));
				KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> WnodeHeader.Flags: 0x%p\n", __FUNCTION__, dwFlags));
				//
				// WNODE_FLAG_FIXED_INSTANCE_SIZE flag is not set
				//
				if ((dwFlags&WNODE_FLAG_ALL_DATA) == WNODE_FLAG_ALL_DATA)
				{
					ULONG dwBlockOffset = pAllData->OffsetInstanceDataAndLength->OffsetInstanceData;
					ULONG dwBlockSize = pAllData->OffsetInstanceDataAndLength->LengthInstanceData;
					KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> DataBlockOffset: 0x%08x\n", __FUNCTION__, dwDataBlockOffset));
					KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> InstanceCount: 0x%08x\n", __FUNCTION__, pAllData->InstanceCount));
					if ((pAllData->WnodeHeader.Flags&WNODE_FLAG_FIXED_INSTANCE_SIZE) == WNODE_FLAG_FIXED_INSTANCE_SIZE)
						dwFixedInstsz = pAllData->FixedInstanceSize;
					else
						//
						// Ignore fix instance size
						//
						dwFixedInstsz = 0;
					KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> Fixed instance size: 0x%08x\n", __FUNCTION__, dwFixedInstsz));
					KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> Offset instance data: 0x%08x\n", __FUNCTION__, dwBlockOffset));
					KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> Length instance data: 0x%08x\n", __FUNCTION__, dwBlockSize));
					PCHAR pDataBlock = (PCHAR)pAllData + dwBlockOffset;

					// on WinXP SP3, the string artifacts begins at offset 0x20
					unsigned int offset = 0x20;
					pDataBlock = (PCHAR)pDataBlock + offset;
					DbgPrint("[%s] BIOS manufacturer: %s\n", __FUNCTION__, pDataBlock);

					//
					// Coremex malware sample SHA1: 62b5427b10f70aeac835a20e71ab0d22dd313e71
					// Find VMware related artifacts 
					// In Win32_BaseBoard, these are the targeted attributes:
					// - Manufacturer = Intel Coropration (VMware WinXP/Win7)
					// - Product = 440BX Desktop Reference Platform (VMware WinXP/Win 7)
					// - SerialNumber = None (VMware WinXP/Win 7)
					// - Version = None (VMware WinXP/Win 7)
					// 
					{
						PCHAR subStr;
						PCHAR tempDataBlock = pDataBlock;
						char *strPattern = "440BX Desktop Reference Platform";

						subStr = strstr(tempDataBlock, strPattern);
						do{

							if (subStr != NULL)
							{
								KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> Found \"%s\"\n", __FUNCTION__, strPattern));
								memcpy((PCHAR)subStr, "44oBX Deskt0p R3f3rence Pl@tf0rm", strlen(strPattern));
								tempDataBlock += (unsigned int)subStr - (unsigned int)tempDataBlock;
							}
							else
							{
								// If no VMware related string found in this block, 
								// find the next string block
								char *pEndBlock = strrchr(tempDataBlock, '\0');
								tempDataBlock = pEndBlock + 1;
							}

							subStr = strstr(tempDataBlock, strPattern);

						} while (tempDataBlock < pDataBlock + dwBlockSize - offset);
					}// end patching Win32_BaseBoard WMI related artifacts

					//
					// Find VMware related artifacts
					// In Win32_BIOS, these are the targeted attributes:
					// - Manufacturer
					// - Serial number
					// - Version
					//
						{
							PCHAR subStr;
							PCHAR tempDataBlock = pDataBlock;
							char *strVmPattern = "VMware";

							subStr = strstr(tempDataBlock, strVmPattern);
							do{

								if (subStr != NULL)
								{
									KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> Found vmware string\n", __FUNCTION__));
									memcpy((PCHAR)subStr, "MYTUAL", 6);
									tempDataBlock += (unsigned int)subStr - (unsigned int)tempDataBlock;
								}
								else
								{
									// If no VMware related string found in this block, 
									// find the next string block
									char *pEndBlock = strrchr(tempDataBlock, '\0');
									tempDataBlock = pEndBlock + 1;
								}

								subStr = strstr(tempDataBlock, strVmPattern);

							} while (tempDataBlock < pDataBlock + dwBlockSize - offset);
						}// end patching VMware string

						//
						// Find virtual related string
						//
						{
							PCHAR subStr;
							PCHAR tempDataBlock = pDataBlock;
							char *strVmPattern = "Virtual";

							subStr = strstr(tempDataBlock, strVmPattern);
							do{

								if (subStr != NULL)
								{
									KdPrint(("[%s] IRP_MN_QUERY_ALL_DATA -> Found virtual string\n", __FUNCTION__));
									memcpy((PCHAR)subStr, "MYVTUAL", 7);
									tempDataBlock += (unsigned int)subStr - (unsigned int)tempDataBlock;
								}
								else
								{
									// If no VMware related string found in this block, 
									// find the next string block
									char *pEndBlock = strrchr(tempDataBlock, '\0');
									tempDataBlock = pEndBlock + 1;
								}

								subStr = strstr(tempDataBlock, strVmPattern);

							} while (tempDataBlock < pDataBlock + dwBlockSize - offset);
						}// end patching Virtual string

				}
				break;
			}
			default:
			{
				KdPrint(("[%s] Not handled this minor function\n", __FUNCTION__));
				break;
			}
			}
		}

	}

	//
	// Mark the Irp pending if required
	//
	if (Irp->PendingReturned) {

		IoMarkIrpPending(Irp);
	}
	return Irp->IoStatus.Status;
}